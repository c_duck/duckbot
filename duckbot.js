const commando = require('discord.js-commando');
const path = require('path');
const oneLine = require('common-tags').oneLine;
const sqlite = require('sqlite');

// Get authentication data
try {
    var AuthDetails = require("./auth.json");
} catch (e){
    console.log("Please create an auth.json like auth.json.example with a bot token or an email and password.\n"+e.stack);
    process.exit();
}

const token = AuthDetails.bot_token;
const owner = AuthDetails.owner;
const admins = AuthDetails.admins;
const gsheet_api_key = AuthDetails.gsheet_api_key;

const client = new commando.CommandoClient({
    owner: owner,
    admins: admins,
    commandPrefix: '!',
    unknownCommandResponse: false
});

client.countdowns = {
    test: 'foo',
    fest: 'bar'
};

var GoogleSpreadsheet = require('google-spreadsheet');
var creds = require('./client_secret.json');
var doc = new GoogleSpreadsheet(gsheet_api_key);
doc.useServiceAccountAuth(creds, function (err) {
  doc.getInfo(function (err, info){
    var worksheetsstring = 'Worksheets: \n';
    for(var o in info.worksheets) {
      worksheetsstring += info.worksheets[o].id+'\n';
    }
    console.log('Worksheet info: \n' + info.id + '\n' + worksheetsstring);
  });
});

client
    .on('unknownCommand', (msg) => {
      doc.useServiceAccountAuth(creds, function (err) {
        var x=-1;
				doc.getInfo(function (err, info){
          var arr = new Array;
	        for(var o in info.worksheets) {
            arr.push(info.worksheets[o].title);
        	}
          var index = -1;
          if(msg.guild != null){
            index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
          }
          if (index > -1) {
            x = index +1;
          }
          if(x>-1){
            doc.getRows(x, function (err, rows){
              //console.log(rows);
              var array = new Array;
              for(var o in rows) {
                array.push(rows[o].command);
              }
              //console.log(array);
              var content = msg.content.substring(1, msg.content.length);
              var index = array.indexOf(content);
              if (index > -1) {
                msg.channel.sendMessage(rows[index].text);
              }
              else {
                var index = arr.indexOf('commands');
                if (index > -1) {
                  x = index +1;
                }
                if(x>-1){
                  doc.getRows(x, function (err, rows){
                    //console.log(rows);
                    var array = new Array;
                    for(var o in rows) {
                      array.push(rows[o].command);
                    }
                    //console.log(array);
                    var content = msg.content.substring(1, msg.content.length);
                    var index = array.indexOf(content);
                    if (index > -1) {
                      msg.channel.sendMessage(rows[index].text);
                    }
                  });
                }
              }
            });
          }
        });
      });
    })
    .on('error', console.error)
    .on('warn', console.warn)
    .on('debug', console.log)
    .on('ready', () => {
        console.log(`Client ready; logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`);
        console.log("Serving in " + client.guilds.array() + " servers");
    })
    .on('disconnect', () => { console.warn('Disconnected!'); })
    .on('reconnect', () => { console.warn('Reconnecting...'); })
    .on('commandError', (cmd, err) => {
        if(err instanceof commando.FriendlyError) return;
        console.error(`Error in command ${cmd.groupID}:${cmd.memberName}`, err);
    })
    .on('commandBlocked', (msg, reason) => {
        console.log(oneLine`
            Command ${msg.command ? `${msg.command.groupID}:${msg.command.memberName}` : ''}
            blocked; ${reason}
        `);
    })
    .on('commandPrefixChange', (guild, prefix) => {
        console.log(oneLine`
            Prefix ${prefix === '' ? 'removed' : `changed to ${prefix || 'the default'}`}
            ${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
        `);
    })
    .on('commandStatusChange', (guild, command, enabled) => {
        console.log(oneLine`
            Command ${command.groupID}:${command.memberName}
            ${enabled ? 'enabled' : 'disabled'}
            ${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
        `);
    })
    .on('groupStatusChange', (guild, group, enabled) => {
        console.log(oneLine`
            Group ${group.id}
            ${enabled ? 'enabled' : 'disabled'}
            ${guild ? `in guild ${guild.name} (${guild.id})` : 'globally'}.
        `);
    })
    .on('guildCreate', (guild) => {
      console.log('joined guild: ' + guild.name + ' with id: ' + guild.id);
      doc.useServiceAccountAuth(creds, function (err) {
        doc.addWorksheet({'title': guild.name + ' ' + guild.id, 'headers': ['command','text']},function (err) {
        });
        var x=-1;
				doc.getInfo(function (err, info){
          var arr = new Array;
	        for(var o in info.worksheets) {
            arr.push(info.worksheets[o].title);
        	}
          var index = arr.indexOf(guild.name + ' ' + guild.id);
          if (index > -1) {
            x = index +1;
          }
          console.log('x '+ x);
          if(x>-1){
            doc.addRow(x, {command: 'deploy', text: 'no deployment set yet'}, function(err) {
              if(err) {
                console.log(err);
              }
            });
          }
				});
      });
    })
    //.on('guildMemberAdd', (guildMember) => {
    //  guildMember.guild.defaultChannel.sendMessage('Welcome to ' + guildMember.guild.name + ' ' + guildMember.displayName + '!');
    //})
    ;

client.setProvider(
    sqlite.open(path.join(__dirname, 'settings.sqlite3')).then(db => new commando.SQLiteProvider(db))
).catch(console.error);

client.registry
    .registerGroups([
        ['math', 'Math'],
        ['random', 'Random'],
        ['fun', 'Fun commands'],
        ['utility', 'Utility commands'],
        ['countdown', 'Countdown commands'],
        ['admin', 'Admin commands'],
        ['vmcommands', 'VM commands'],
        ['textcommands', 'Text commands']
    ])
    .registerDefaults()
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.login(token);
client.AuthDetails = AuthDetails;
