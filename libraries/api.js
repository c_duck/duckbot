exports.GetToons = function(msg, toon, guild, stars, onlyZetas = false) {
    if(toon && guild && stars) {
        var request = require('request');
        var url = "https://ventress-19de0.firebaseio.com/" + guild + ".json?orderBy=%22ID%22&equalTo=%22" + toon + "%22&auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";

        request(url, function (error, response, body) {
            if (error) {
                msg.channel.send("couldn't find toon");
            } else {
                var json = JSON.parse(body);
                var key = Object.keys(json)[0];
                if(key == "error"){
                    msg.channel.send("couldn't find guild");
                }
                else if(key == undefined){
                    msg.channel.send("couldn't find toon");
                }
                else {
                    if(json[key].Players != undefined) {
                        var columnify = require('columnify');
                        msg.channel.send("```Toon: " + toon + " Guild: " + guild + "```");
                        var data0 = json[key].Players;
                        data0 = data0.sort(function(a,b) {
                          return b.Power-a.Power;
                        });
                        var datalength = data0.length;
                        var data = [];
                        var count = 1;
                        for (var i = 0; i < datalength;i++) {
                            data0[i]['No']=count;
                            if (data0[i].Rarity >= stars) {
                                //if onlyZetas only return toons that have zetas
                                if(onlyZetas) {
                                    if(data0[i].Zetas && data0[i].Zetas.length > 0) {
                                        data.push(data0[i]);
                                        count+=1;
                                    }
                                } else {
                                    data.push(data0[i]);
                                    count+=1;
                                }

                            }
                        }
                        datalength = data.length;
                        var x = 0;
                        var dataslice;
                        var slicesice = 25;
                        var headers = true;
                        while (x < datalength){
                            dataslice = data.slice(x, x + slicesice);
                            var columns = columnify(dataslice, {
                                include: ['No', 'PlayerName', 'Rarity', 'ToonLevel', 'GearLevel', 'Power', 'Zetas'],
                                showHeaders: headers,
                                    config: {
                                        'No': {minWidth: 2, maxWidth: 2, align: 'right'},
                                        'PlayerName': {minWidth: 18, maxWidth: 18},
                                        'Rarity': {
                                                headingTransform: function(heading) {
                                        return "STARS"
                                                },
                                            minWidth: 5, maxWidth: 5, align: 'right'
                                        },
                                        'ToonLevel': {
                                                headingTransform: function(heading) {
                                        return "LEVEL"
                                                },
                                            minWidth: 5, maxWidth: 5, align: 'right'},
                                        'GearLevel': {
                                                headingTransform: function(heading) {
                                        return "GEAR"
                                                },
                                            minWidth: 4, maxWidth: 4, align: 'right'},
                                        'Power': {
                                            minWidth: 5, maxWidth: 5, align: 'right'}
                                    }
                                }
                            );
                            msg.channel.send("```" + columns + "```");
                            x=x+slicesice;
                            headers=false;
                        }
                    }
                    else {msg.channel.send("no one has this toon")}
                }
            }
        });
    }

}

exports.GetGuildFromParms = function(guildName, arg1, arg2) {
    var guild = '';

    if(arg1 && isNaN(parseInt(arg1))) {
        //if argument 1 isn't a number it's guild
        if (arg1 == 'detect') {
            guild = guildName.match(/\b(\w)/g).join('').toLowerCase();
        }
        else {
            guild = arg1;
        }
    } else if(arg2 && isNaN(parseInt(arg2))) {
        //if argument 2 isn't a number it's guild
        if (arg2 == 'detect') {
            guild = guildName.match(/\b(\w)/g).join('').toLowerCase();
        }
        else {
            guild = arg2;
        }
    } else {
        //if they were both numbers we have a problem
        guild = guildName.match(/\b(\w)/g).join('').toLowerCase();
    }
    if(guild == 'o' || guild == 'opp' || guild == 'tw'){
        guild = 'opponent';
    }

    return guild;
}

exports.GetStarsFromParms = function(arg1, arg2) {
    var stars = 1;
    if (arg1 && !isNaN(parseInt(arg1))){
        stars = parseInt(arg1);
    } else if(arg2 && !isNaN(parseInt(arg2))){
        stars = parseInt(arg2);
    } else {
        stars = 1;
    }

    return stars;
}

exports.GetToonNames = function(msg) {
    var request = require('request');
    var url = "https://ventress-19de0.firebaseio.com/names.json?orderBy=%22Abbreviation%22&auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";
    request(url, function (error, response, body) {
        if (error) {
            msg.channel.send("couldn't find toon");
        }
        else {
            var data = JSON.parse(body);
            var datalength = data.length;
            var x=0;
            var columnify = require('columnify');
            var dataslice;
            var slicesice=35;
            while (x<datalength){
                dataslice=data.slice(x,x+slicesice);
                var columns = columnify(dataslice,
                    {include: ['Abbreviation', 'FullName'],
                    config: {'Abbreviation': {minWidth: 15, maxWidth: 15}}
                    }
                );
                msg.author.send("```"+columns+"```");
                x=x+slicesice;
            }
        }
    });
}

exports.GetUsersFromParms = function(msg, args) {
  return msg.channel.guild.members.filter((member) => member.user.username.toLowerCase().includes(args)).array();
}

exports.UpdateGuildLink = function(msg, guild, link) {
    var request = require('request');
    var url = "https://ventress-19de0.firebaseio.com/guilds.json?auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";

    request(url, function (error, response, body) {
        if (error) {
            console.log(error);
            msg.channel.send("couldn't find guilds");
        }
        else {

            var data = JSON.parse(body);
            console.log(data.length);

            //loop through guilds and update the link to the right guild
            for(var i = 0; i < data.length; i++) {

                if(data[i].StorageBucket == guild) {
                    var id = link.replace("https://swgoh.gg/g/", "").split("/")[0];
                    console.log(id);
                    var g = link.replace("https://swgoh.gg/g/" + id + "/", "").replace("/", "");
                    console.log(g);

                    var z = "https://swgoh.gg/g/[id]/[guild]/zetas/".replace("[id]", id).replace("[guild]", g);
                    var u = "https://swgoh.gg/api/guilds/[id]/units/".replace("[id]", id);

                    data[i].ZetaLink = z;
                    data[i].SwgohLink = link;
                    data[i].LastSync = new Date('1/1/2001');
                    data[i].UnitLink = u;
                }
            }

            console.log(data);

            request.put({
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                url: "https://ventress-19de0.firebaseio.com/guilds.json?auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn",
                body: JSON.stringify(data)
              }, function(error, response, body){
                  msg.channel.send("Guild updated. Syncing will occur within the next 15 minutes...")
                console.log(body);
              });
            //update links
        }
    });
}

exports.RefreshGuildSync = function(msg, guild) {
    var request = require('request');
    var url = "https://ventress-19de0.firebaseio.com/guilds.json?auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";

    request(url, function (error, response, body) {
        if (error) {
            console.log(error);
            msg.channel.send("couldn't find guilds");
        }
        else {

            var data = JSON.parse(body);

            //loop through guilds and update the syncing
            for(var i = 0; i < data.length; i++) {
                if(data[i].StorageBucket == guild) {
                    data[i].LastSync = new Date('1/1/2001');
                }
            }

            request.put({
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                url: "https://ventress-19de0.firebaseio.com/guilds.json?auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn",
                body: JSON.stringify(data)
              }, function(error, response, body){
                  msg.channel.send("Guild added to refresh queue. Please allow up to 15 minutes for syncing...")
              });
            //update links
        }
    });
}

exports.GetPlayerZetas = function(msg, guild, player) {
    var request = require('request');
    var url = "https://ventress-19de0.firebaseio.com/" + guild + "-zetas.json?auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";
    var playerFound = false;
    var zetas = [];
    var zetaCount = 0;

    request(url, function (error, response, body) {
        if (error) {
            console.log(error);
            msg.channel.send("couldn't find guilds");
        }
        else {

            var data = JSON.parse(body);

            var foundZeta = {};

            for(var i = 0; i < data.length; i++) {

                if(data[i].PlayerName == player) {
                    playerFound = true;
                    foundZeta.Toon = data[i].ToonName;
                    zetaCount += data[i].Abilities.length;
                    foundZeta.Zetas = data[i].Abilities.toString();

                    zetas.push(foundZeta)
                    foundZeta = {};
                }
            }

            //if we found a player return the grid
            if(playerFound) {
                var columnify = require('columnify');

                msg.channel.send("```Player: " + player + " Guild: " + guild + " Count: " + zetaCount + "```");

                console.log(zetas);

                var columns = columnify(zetas,
                    {include: ['Toon', 'Zetas'],
                    config: {'Toon': {minWidth: 15, maxWidth: 30}}
                    }
                );
                msg.channel.send("```"+columns+"```");
            } else {
                msg.channel.send("couldn't find any zetas for that player. please validate player spelling");
            }

        }
    });
}

exports.GetRecruitCharacters = function(msg, recruit) {
    const swgoh = require("swgoh").swgoh
    var gp = 0;

    var toonsWeWant = ["Rey (Jedi Training)", "BB-8", "R2-D2", "Commander Luke Skywalker",
        "Captain Han Solo", "Rebel Officer Leia Organa", "Hoth Rebel Soldier", "Hoth Rebel Scout",
        "Jyn Erso", "Chirrut Îmwe", "Baze Malbus", "Ezra Bridger", "Chopper",  "Hera Syndulla",
        "Sabine Wren", "Garazeb \"Zeb\" Orrelios", "Kanan Jarrus", "Grand Admiral Thrawn",
        "Boba Fett", "Zam Wesell", "Colonel Starck", "General Veers", "Snowtrooper", "Shoretrooper",
        "Death Trooper", "Stormtrooper", "Imperial Probe Droid", "Mother Talzin", "Asajj Ventress",
        "Han Solo", "Pao"];

    var toons = []; //the toons we're looking for
    var toon = {};

    swgoh.collection(recruit).then(function (data) {
        var columnify = require('columnify');

        for(var i = 0; i < data.length; i++) {

            var toonindex = toonsWeWant.indexOf(data[i].description);
            if(toonindex > -1) {
                //we found a toon we want. push it to the list of toons we're interested in
                toon.No = toonindex + 1;
                toon.Name = data[i].description;
                toon.Star = data[i].star;
                toon.Power = data[i].galacticPower;
                toon.Gear = data[i].gearLevel;
                toon.Level = data[i].level;
                toons.push(toon);
                gp += data[i].galacticPower;
            }

            toon = {};
        }
        function compare(a,b) {
          if (a.No < b.No)
            return -1;
          if (a.No > b.No)
            return 1;
          return 0;
        }

        toons.sort(compare);

        msg.channel.send("```Recruit: " + recruit + " Character GP: " + gp + "```");

        var columns = columnify(toons,
            {include: ['No', 'Name', 'Star', 'Level', 'Gear', 'Power' ],
            config: {'Name': {minWidth: 15, maxWidth: 30}}
            }
        );

        msg.channel.send("```"+columns+"```");
      })
}

exports.GetRecruitShips = function(msg, recruit) {
    const swgoh = require("swgoh").swgoh
    var gp = 0;

    var toonsWeWant = ["Chimaera", "Executrix"];

    var toons = []; //the toons we're looking for
    var toon = {};

    swgoh.ship(recruit).then(function (data) {
        var columnify = require('columnify');

        for(var i = 0; i < data.length; i++) {

            console.log(data[i].description);

            var toonindex = toonsWeWant.indexOf(data[i].description);
            if(toonindex > -1) {
                //we found a toon we want. push it to the list of toons we're interested in
                toon.No = toonindex + 1;
                toon.Name = data[i].description;
                toon.Star = data[i].star;
                toon.Power = data[i].galacticPower;
                toon.Gear = 0;
                toon.Level = data[i].level;
                toons.push(toon);
                gp += data[i].galacticPower;
            }

            toon = {};
        }
        function compare(a,b) {
          if (a.No < b.No)
            return -1;
          if (a.No > b.No)
            return 1;
          return 0;
        }

        toons.sort(compare);


        msg.channel.send("```Recruit: " + recruit + " Ship GP: " + gp + "```");

        var columns = columnify(toons,
            {include: ['No', 'Name', 'Star', 'Level', 'Gear', 'Power' ],
            config: {'Name': {minWidth: 15, maxWidth: 30}}
            }
        );

        msg.channel.send("```"+columns+"```");
      })
}
