exports.StripSpecialCharacters = function(value) {
    return value.replace(/[^a-zA-Z0-9]/g, "");
}

exports.GetGuildBucketName = function(input) {
    var guild = "";

    switch(input) {
        case "moon":
        case "vm":
            guild = "vm";
            break;
        case "sun":
        case "vs":
            guild = "vs";
            break;
        case "carnival":
        case "vc":
            guild = "vc";
            break;
        case "qui":
        case "vq":
            guild = "vq";
            break;
        case "white squadron":
        case "ws":
        case "vws":
            guild = "vws";
            break;
        case "opp":
        case "tw":
        case "opponent":
            guild = "opponent";
            break;
        default:
            guild = "vm";
    }

    //testing
    console.log("guild: " + guild);

    return guild;
}