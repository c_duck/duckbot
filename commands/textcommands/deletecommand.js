const commando = require('discord.js-commando');

module.exports = class DeleteCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'deletecommand',
			aliases: ['delcom'],
			group: 'textcommands',
			memberName: 'deletecommand',
			description: 'Delete a simple text command',
			details: 'Delete a text command - !delcom commandname',
			argsType: 'multiple',
			argsSingleQuotes: true,
      guildOnly: true
		});
	}

	async run(msg, args) {
		if (args.length > 0) {
			var GoogleSpreadsheet = require('google-spreadsheet');
			var creds = require('../../client_secret.json');
			var AuthDetails = require('../../auth.json');
			var doc = new GoogleSpreadsheet(AuthDetails.gsheet_api_key);
			var command = args.shift();
			console.log('command ' + command);
			doc.useServiceAccountAuth(creds, function (err) {
				var x=-1;
				doc.getInfo(function (err, info){
          var arr = new Array;
	        for(var o in info.worksheets) {
            arr.push(info.worksheets[o].title);
        	}
          var index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
          if (index > -1) {
            x = index +1;
						doc.getRows(x, function (err, rows){
							var array = new Array;
							for(var o in rows) {
								array.push(rows[o].command);
							}
							var index = array.indexOf(command);
							if (index > -1) {
								rows[index].del();
								msg.channel.send('Command deleted: ' + command);
							}
							else {
                var index = arr.indexOf('commands');
                if (index > -1) {
                  x = index +1;
                  doc.getRows(x, function (err, rows){
                    //console.log(rows);
                    var array = new Array;
                    for(var o in rows) {
                      array.push(rows[o].command);
                    }
                    var index = array.indexOf(command);
                    if (index > -1) {
											rows[index].del();
											msg.channel.send('General command deleted: ' + command);
                    }
										else {
											msg.channel.send('No such command exist');
										}
                  });
                }
              }
						});
					}
				});
			});
		}
		else {
			msg.channel.send('Please input a command to delete');
		}
	}
}
