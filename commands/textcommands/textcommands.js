const commando = require('discord.js-commando');

module.exports = class Textcommands extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'textcommands',
			aliases: ['textcoms'],
			group: 'textcommands',
			memberName: 'textcommands',
			description: 'List all textcommands added by !addcommand ',
			details: 'List all commands added by !addcommand',
      guildOnly: true
		});
	}

	async run(msg, args) {
		var GoogleSpreadsheet = require('google-spreadsheet');
		var creds = require('../../client_secret.json');
		var AuthDetails = require('../../auth.json');
		var doc = new GoogleSpreadsheet(AuthDetails.gsheet_api_key);
		doc.useServiceAccountAuth(creds, function (err) {
			var x=-1;
			doc.getInfo(function (err, info){
				var arr = new Array;
				for(var o in info.worksheets) {
					arr.push(info.worksheets[o].title);
				}
				var index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
				console.log(index);
				if (index > -1) {
					x = index +1;
				}
				if(x>-1){
					doc.getRows(x, function (err, rows){
						//var array = new Array;
						var commands = "__Text commands__";
						for(var o in rows) {
							commands = commands + "\n" + rows[o].command;
						}
						msg.member.send(commands);
					});
				}
			});
		});
	}
}
