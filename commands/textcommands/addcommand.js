const commando = require('discord.js-commando');

module.exports = class AddCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'addcommand',
			aliases: ['addcom'],
			group: 'textcommands',
			memberName: 'addcommand',
			description: 'Add a simple text command to duckbot (server specific)',
			details: 'Add a text command - !addcom commandname response',
			argsType: 'multiple',
			argsSingleQuotes: true,
      guildOnly: true
		});
	}

	async run(msg, args) {
		console.log(args);
		if (args.length > 1) {
			var GoogleSpreadsheet = require('google-spreadsheet');
			var creds = require('../../client_secret.json');
			var AuthDetails = require('../../auth.json');
			var doc = new GoogleSpreadsheet(AuthDetails.gsheet_api_key);
			var command = args.shift();
			var text = args.join(' ');
			console.log('command ' + command);
			console.log('text ' + text);
			doc.useServiceAccountAuth(creds, function (err) {
				var x=-1;
				doc.getInfo(function (err, info){
          var arr = new Array;
	        for(var o in info.worksheets) {
            arr.push(info.worksheets[o].title);
        	}
          var index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
          if (index > -1) {
            x = index +1;
						doc.addRow(x, {command: command, text: text}, function(err) {
							msg.channel.send('Command added: ' + command);
							if(err) {
			    			console.log(err);
			  			}
						});
					}
				});
			});
		}
		else {
			msg.channel.send('Please input command and command response: !addcom command command response');
		}
	}
}
