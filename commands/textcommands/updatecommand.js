const commando = require('discord.js-commando');

module.exports = class UpdateCommand extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'updatecommand',
			aliases: ['updcom'],
			group: 'textcommands',
			memberName: 'updatecommand',
			description: 'Update a simple text command to duckbot',
			details: 'Update a text command - !updcom commandname response',
			argsType: 'multiple',
			argsSingleQuotes: true,
      guildOnly: true
		});
	}

	async run(msg, args) {
		if (args.length > 1) {
			var GoogleSpreadsheet = require('google-spreadsheet');
			var creds = require('../../client_secret.json');
			var AuthDetails = require('../../auth.json');
			var doc = new GoogleSpreadsheet(AuthDetails.gsheet_api_key);
			var command = args.shift();
			var text = args.join(' ');
			console.log('command ' + command);
			console.log('text ' + text);
			doc.useServiceAccountAuth(creds, function (err) {
				var x=-1;
				doc.getInfo(function (err, info){
          var arr = new Array;
	        for(var o in info.worksheets) {
            arr.push(info.worksheets[o].title);
        	}
          var index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
          if (index > -1) {
            x = index +1;
						doc.getRows(x, function (err, rows){
							var array = new Array;
							for(var o in rows) {
								array.push(rows[o].command);
							}
							var index = array.indexOf(command);
							if (index > -1) {
								rows[index].text = text;
								rows[index].save();
								msg.channel.send('Command updated: ' + command);
							}
							else{
								var index = arr.indexOf('commands');
                if (index > -1) {
                  x = index +1;
                  doc.getRows(x, function (err, rows){
										var array = new Array;
										for(var o in rows) {
											array.push(rows[o].command);
										}
										var index = array.indexOf(command);
										if (index > -1) {
											rows[index].text = text;
											rows[index].save();
											msg.channel.send('General command updated: ' + command);
										}
										else {
											msg.channel.send('No such command exist');
										}
									});
								}
							}
						});
					}
				});
			});
		}
		else {
			msg.channel.send('Please input command and command response: !addcom command command response');
		}
	}
}
