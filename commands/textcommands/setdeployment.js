const commando = require('discord.js-commando');

module.exports = class SetDeployment extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'setdeployment',
			aliases: ['setdeploy', 'setd'],
			group: 'textcommands',
			memberName: 'setdeployment',
			description: 'Set deployment instructions',
			details: 'Set deployment instructions',
      guildOnly: true
		});
	}

	async run(msg, args) {
		var GoogleSpreadsheet = require('google-spreadsheet');
		var creds = require('../../client_secret.json');
		var AuthDetails = require('../../auth.json');
		var doc = new GoogleSpreadsheet(AuthDetails.gsheet_api_key);
		doc.useServiceAccountAuth(creds, function (err) {
			var x=-1;
			doc.getInfo(function (err, info){
				var arr = new Array;
				for(var o in info.worksheets) {
					arr.push(info.worksheets[o].title);
				}
				var index = arr.indexOf(msg.guild.name + ' ' + msg.guild.id);
				if (index > -1) {
					x = index +1;
					doc.getRows(x, function (err, rows){
						//console.log(rows);
						var array = new Array;
						for(var o in rows) {
							array.push(rows[o].command);
						}
						//console.log(array);
						var index = array.indexOf('deploy');
						if (index > -1) {
							rows[index].text = args;
							rows[index].save();
							msg.channel.send('New deployment instructions set');
						}
						else {
							msg.channel.send('No such command exist');
						}
					});
				}
			});
		});
	}
}
