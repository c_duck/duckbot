const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class CancelCountdown extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'cancelcountdown',
			aliases: ['cancelcd', 'cancel'],
			group: 'countdown',
			memberName: 'cancelcountdown',
			description: 'Cancel a specific countdown',
			examples: [`!cancelcountdown some event`, `!cancelcd 'some event'`],
			args: [
				{
					key: 'event',
					label: 'eventname',
					prompt: 'Please input a countdown event that you want to cancel',
					type: 'string'
				}
			],
			guildOnly: true
		});
	}

	async run(msg, args) {
		//msg.channel.send("Args " + args.event);
        var cdid=msg.guild.id+"_"+args.event;
        if(this.client.countdowns[cdid]===undefined) {
           msg.channel.send("No such countdown exist");
        }
        else {
            this.client.countdowns[cdid].cancelCountdown(msg);
        }
	}
}
