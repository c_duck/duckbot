const commando = require('discord.js-commando');
const Tout = require('./tout.js');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class CancelRotations extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'cancelrotations',
			aliases: ['cancelrots'],
			group: 'countdown',
			memberName: 'cancelrotations',
			description: 'Cancels all rotations on this server',
      guildOnly: true
		});
	}

	async run(msg, args) {

		var substring_of_id = msg.guild.id;
		//var keys = Object.keys(this.client.countdowns);
		//var keys_to_cancel = [];
		Object.keys(this.client.countdowns).forEach(function(trait) {
			if(trait.indexOf(substring_of_id) > -1) {
      	msg.client.countdowns[trait].cancelTimeout();
			}
		});

	}

}
