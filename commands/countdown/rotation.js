const commando = require('discord.js-commando');
const Tout = require('./tout.js');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Rotation extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'rotation',
			aliases: ['rot'],
			group: 'countdown',
			memberName: 'rotation',
			description: 'Adds a daily rotation message',
      details: 'Adds a daily reocurring message. Use 3+ arguments: \n1. message time - UTC time \n2. rotation direction boolean if the rotation should go up (3->2->1) otherwise down (1->2->3) \n3+. The participants',
			argsType: 'multiple',
			argsSingleQuotes: true,
      guildOnly: true
		});
	}

	async run(msg, args) {
		var len = args.length;
		var people = len - 2;
    //console.log(len);
		var nowms=Date.now();
		var now=new Date(nowms);
		var thenh=Math.floor(args[0]);
		var thenm=Math.round(args[0]%1*100);
		var mins=thenm-now.getUTCMinutes();
		var hadjust=0;
		if(mins<=0) {
				hadjust=-1;
				mins=mins+60;
		}
		var hours=(hadjust+24+thenh-now.getUTCHours())%24;
		var offsetms=now.getUTCSeconds()*1000+now.getUTCMilliseconds();
		var ms = (hours*60+mins)*60*1000-offsetms;

		//console.log('hours ' + hours);
		//console.log('mins ' + mins);
		//console.log('ms ' + ms);

		if(len<3){
			msg.channel.send('please input at least 3 arguments. Message time, up or down, and names');
		}
		else if(isNaN(args[0])){
			msg.channel.send('error, please input a number as first argument');
		}
		else if(args[0]<0 || args[0]>=24){
			msg.channel.send('please input a number between 0-23.59');
		}
		else if(args[1]=='u' || args[1]=='d' || args[1]=='up' || args[1]=='down'){



			for(var k=2; k<len; k++){
				var startnumber=k-1;
				//console.log(startnumber + '. ' + args[k]);
				msg.channel.send(startnumber + '. ' + args[k]).then(message => message.delete(ms));

				var j=0;
				if(args[1]=='d' || args[1]=='down'){
		      for(var i=startnumber+1; i<=people+startnumber; i++) {
		        var position = i%people;
		        if(position==0){position=people;}
						var cdid = msg.guild.id + "_" + args[k] + "_" + [args[0]] + "_" + people + "_" + position;
						var serverid=msg.guild.id;
		        //console.log('position ' + position);
		        var newmessage = position + '. ' + args[k];
		        var mslag=(position-1)*1000;
		        //console.log('mslag ' + mslag);
		  		  this.client.countdowns[cdid] = new Tout(this.client, msg, newmessage, args[0], j, people*24, 24, mslag, cdid, serverid);
		        j++;
		  		  this.client.countdowns[cdid].startTimeout(msg);
		  		  this.client.countdowns[cdid].guildId=serverid;
		  		  this.client.countdowns[cdid].guildName=msg.guild.name;
		      }
		    }
		    else {
		      for(var i=startnumber-1; i>=startnumber-people; i--) {
		        var position = (people+i)%people;
		        if(position==0){position=people;}
						var cdid = msg.guild.id + "_" + args[k] + "_" + [args[0]] + "_" + people + "_" + position;
						var serverid=msg.guild.id;
		        //console.log('position ' + position);
		        var newmessage = position + '. ' + args[k];
		        var mslag=(position-1)*1000;
		        //console.log('mslag ' + mslag);
		  		  this.client.countdowns[cdid] = new Tout(this.client, msg, newmessage, args[0], j, people*24, 24, mslag, cdid, serverid);
		        j++;
		  		  this.client.countdowns[cdid].startTimeout(msg);
		  		  this.client.countdowns[cdid].guildId=serverid;
		  		  this.client.countdowns[cdid].guildName=msg.guild.name;
		      }
		    }
			}
		}
		else {
			msg.channel.send('please input u, d, up, or down (lower case) as second argument');
		}
    //															 constructor(client, msg, name, messagetime, daydelay, mslag, duration, timeouttime, timeoutid, serverid)
	}

}
