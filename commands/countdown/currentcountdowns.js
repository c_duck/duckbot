const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class CurrentCountdowns extends commando.Command {
		constructor(client) {
				super(client, {
					name: 'currentcountdowns',
					aliases: ['currentcds', 'currcds'],
					group: 'countdown',
					memberName: 'currentcountdowns',
					description: 'Get current countowns',
					guildOnly: true
				});
		}

		async run(msg, args) {
				var text = "current countdowns:\n";
		    var serverid=msg.guild.id;
		    var countdown;
		    for(var cdid in this.client.countdowns){
		        if(typeof cdid === 'string') {
		            if(this.client.countdowns[cdid].serverid==serverid) {
		                countdown=this.client.countdowns[cdid].eventname;
		                text += countdown + "\t" + this.client.countdowns[cdid].getTimeLeft() + "\n";
		            }
		        }
		    }
		    msg.channel.send(text);
		}
}
