const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class CancelPayout extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'cancelpayout',
			aliases: ['cancelpo'],
			group: 'countdown',
			memberName: 'cancelpayout',
			description: 'Cancel a specific payout',
			args: [
				{
					key: 'event',
					label: 'eventname',
					prompt: 'Please input a countdown event that you want to cancel',
					type: 'string'
				}
			],
			guildOnly: true
		});
	}

	async run(msg, args) {
		msg.channel.send("Args " + args.event);
        var cdid=msg.guild.id+"_"+args.event;
        if(this.client.countdowns[cdid]===undefined) {
           msg.channel.send("No such countdown exist");
        }
        else {
            this.client.countdowns[cdid].cancelPayout(msg);
        }
	}
}
