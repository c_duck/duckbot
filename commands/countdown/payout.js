const commando = require('discord.js-commando');
const Pout = require('./pout.js');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Payout extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'payout',
			aliases: ['po'],
			group: 'countdown',
			memberName: 'payout',
			description: 'Keeps track of a specific player´s payout',
			details: 'Create a timer for a specific player payout. Use 2 arguments: \n1. name - (string) \n2. toTime - UTC time for payout (number)',
			examples: [`!payout player name 17`, `!po 'player name' 1'`],
            args: [
                {
                    key: 'name',
                    label: 'name (string)',
                    prompt: 'Please input the name of the player',
                    type: 'string'
                },
                {
                    key: 'toTime',
                    label: 'toTime (number)',
                    prompt: 'Please input a time to countdown to',
                    type: 'float',
                    min: 0,
                    max: 24
                }
            ],
            argsSingleQuotes: true,
            argsCount: 2,
            guildOnly: true
		});
	}

	async run(msg, args) {
		var name = args.name;
		var totime = args.toTime;
    var cdid = msg.guild.id + "_" + name;
		var serverid=msg.guild.id;
		this.client.countdowns[cdid] = new Pout(this.client, msg, name, totime, cdid, serverid);
		this.client.countdowns[cdid].startCountdown(msg);
		this.client.countdowns[cdid].guildId=serverid;
		this.client.countdowns[cdid].guildName=msg.guild.name;
	}

}
