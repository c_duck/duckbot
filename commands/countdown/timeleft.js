const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class TimeLeft extends commando.Command {
		constructor(client) {
				super(client, {
						name: 'timeleft',
						group: 'countdown',
						memberName: 'timeleft',
						description: 'Gets time left for a specific countdown',
						examples: [`!timeleft some event`, `!timeleft 'some event'`],
						args: [
								{
									key: 'event',
									label: 'eventname',
									prompt: 'Please input a countdown event that you want to check time left for',
									type: 'string'
								}
						],
						guildOnly: true
				});
		}

		async run(msg, args) {
				var cdid = msg.channel.guild.id + "_" + args.event;
				if(this.client.countdowns[cdid]===undefined) {
		           msg.channel.send("No such countdown exist");
        }
        else {
        	msg.channel.send("Time left to " + args.event + " is " + this.client.countdowns[cdid].getTimeLeft());
        }
		}
}
