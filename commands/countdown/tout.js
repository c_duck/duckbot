
module.exports = class Tout {
		constructor(client, msg, name, messagetime, daydelay, timeouttime, duration, mslag, timeoutid, serverid) {
        this.serverid=serverid;
				this.client=client;
				this.eventname = name;
		    this.timeouts = [];
		    this.timeoutno = 0;
        this.intervals = [];
		    this.messagetime=messagetime;
				this.daydelay=daydelay;
				this.duration=duration;
				this.timeouttime=timeouttime;
				this.mslag=mslag;
				//console.log('message2 ' + this.eventname);
				//console.log('messagetime2 ' + this.messagetime);
				//console.log('daydelay2 ' + this.daydelay);
				//console.log('duration2 ' + this.duration);
				//console.log('timeouttime2 ' + this.timeouttime);
				//console.log('mslag2 ' + this.mslag);
		    var nowms=Date.now();
		    var now=new Date(nowms);
				var thenh=Math.floor(messagetime);
		    var thenm=Math.round(messagetime%1*100);
		    var mins=thenm-now.getUTCMinutes();
		    var hadjust=0;
		    if(mins<=0) {
		        hadjust=-1;
		        mins=mins+60;
		    }
		    var hours=(hadjust+24+thenh-now.getUTCHours())%24;
		    var offsetms=now.getUTCSeconds()*1000+now.getUTCMilliseconds();
		    var minutes=this.daydelay*24*60+hours*60+mins-offsetms/(1000*60);
		    this.thenms=nowms+minutes*60*1000;
		    this.then=new Date(this.thenms);
		    this.cdid=timeoutid;
		}
    getMessageTime() {
        return this.messagetime;
    }
    getTimeLeft() {
        var nowms=Date.now();
        var msleft=this.thenms-nowms;
        var hleft=msleft/(1000*60*60); hleft=Math.floor(hleft); msleft=msleft-hleft*60*60*1000;
        var mleft=msleft/(1000*60); mleft=mleft=Math.floor(mleft); msleft=msleft-mleft*60*1000;
        var sleft=msleft/1000; sleft=Math.floor(sleft); msleft=msleft-sleft*1000;
        return hleft+"h"+mleft+"m"+sleft+"s"+msleft+"ms";
    }
    getMsLeft() {
        var nowms=Date.now();
        return this.thenms-nowms;
    }
    addTimeoutMessage(intervalno, message, msgtime, mslag, msg, timeout, deletetime) {
        var nowms=Date.now();
        var timeleft=this.getMsLeft() + mslag;
        if (timeleft<0){timeleft=timeleft+24*60*60*1000}
        //var message = text;
        //var msgtime = new Date(nowms + timeout);
				//console.log("At " + msgtime.getHours()+"h"+msgtime.getMinutes()+"m"+msgtime.getSeconds()+"s"+msgtime.getMilliseconds()+"ms " + message);
        this.timeouts[this.timeoutno]=setTimeout(function(){
          msg.channel.send(message).then(message => message.delete(deletetime*60*60*1000));
        },timeleft);
        this.timeoutno++;

				//console.log('timouttest ' + timeout);
				//console.log('deletetest ' + deletetime);
        var that=this;
        this.timeouts[this.timeoutno]=setTimeout(function(){
          that.intervals[intervalno]=setInterval(function(){
            msg.channel.send(message).then(message => message.delete(deletetime*60*60*1000));
          },timeout*60*60*1000);
        },timeleft);
				this.timeoutno++;
    }
    cancelCountdown(msg) {
      msg.channel.send("This is a timeout message, please cancel with canceltimeout instead");
    }
    cancelTimeout() {
          clearInterval(this.intervals[0]);
        for(var i=0; i<=this.timeoutno; i++) {
          clearTimeout(this.timeouts[i]);
        }
				delete this.client.countdowns[this.cdid];
				console.log("Countdown cancelled");
    }
    logIntervals() {
      console.log(this.intervals);
    }
    logTimeouts() {
      console.log(this.timeouts);
    }
    getTimeoutno() {
        return this.timeoutno;
    }
    startTimeout(msg) {
        var ms = this.getMsLeft();
        var mins = ms/(60*1000);
        var hours = Math.floor(mins/60);
        var firstdel = ms%(60*60*1000);

        var deltime = this.duration;
        this.addTimeoutMessage(0, this.eventname, this.messagetime, this.mslag, msg, this.timeouttime, this.duration);
        //msg.channel.send("New timeout message " + this.eventname + ". Timeout time " + this.timeouttime + " hours.").then(message => message.delete(firstdel));
    }
}
