const commando = require('discord.js-commando');
const CdTo = require('./cdto.js');
var moment = require('moment-timezone');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class CountdownToEST extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'countdowntoest',
			aliases: ['cdtoest'],
			group: 'countdown',
			memberName: 'countdowntoest',
			description: 'Countdown to a specific time (EST) within 24h',
			details: 'Create a countdown to a specific time. Use 3 arguments: \n1. event - (string) \n2. toTime - (number), \n3 everyone/yes/name/no - (string) if you want to notify everyone at 10 and 0 minutes (optional)',
			examples: [`!countdownto someevent 16.30 everyone`, `!cdto 'some event' 21.44'`],
            args: [
                {
                    key: 'event',
                    label: 'eventname (string)',
                    prompt: 'Please input an event to countdown to',
                    type: 'string'
                },
                {
                    key: 'toTime',
                    label: 'toTime (number)',
                    prompt: 'Please input a time to countdown to',
                    type: 'float',
                    min: 0,
                    max: 24
                },
                {
                    key: 'mention',
                    label: 'mention (string)',
                    prompt: 'Do you want to mention someone (type name) or everyone (type yes or everyone) when the countdown is finished (and 10 minutes before)?',
                    type: 'string'
                }
            ],
            argsSingleQuotes: true,
            argsCount: 3,
            guildOnly: true
		});
	}

	async run(msg, args) {

    var now = moment.utc();
    // get the zone offsets for this time, in minutes
    var offset1 = moment.tz.zone("America/New_York").offset(now);
    var offset2 = moment.tz.zone("Europe/Stockholm").offset(now);
		var offseth = offset1/60-offset2/60;

		var event = args.event;
		var totime = args.toTime;
    var mention = args.mention;
    var cdid = msg.guild.id + "_" + event;
		var serverid=msg.guild.id;
		this.client.countdowns[cdid] = new CdTo(this.client, msg, event, totime, -offseth, cdid, serverid);
		this.client.countdowns[cdid].startCountdown(mention, msg);
	}
}
