
module.exports = class Pout {
		constructor(client, msg, name, totime, payoutid, serverid) {
        this.serverid=serverid;
				this.client=client;
				this.eventname = name;
		    this.timeouts = [];
		    this.timeoutno = 0;
        this.intervals = [];
		    this.totime=totime;
		    var nowms=Date.now();
		    var now=new Date(nowms);
				var thenh=Math.floor(totime);
		    var thenm=Math.round(totime%1*100);
		    var mins=thenm-now.getUTCMinutes();
		    var hadjust=0;
		    if(mins<=0) {
		        hadjust=-1;
		        mins=mins+60;
		    }
		    var hours=(hadjust+24+thenh-now.getUTCHours())%24;
		    var offsetms=now.getUTCSeconds()*1000+now.getUTCMilliseconds();
		    var minutes=hours*60+mins-offsetms/(1000*60);
		    this.thenms=nowms+minutes*60*1000;
		    this.then=new Date(this.thenms);
		    this.cdid=payoutid;
		}
    getToTime() {
        return this.totime;
    }
    getTimeLeft() {
        var nowms=Date.now();
        var msleft=this.thenms-nowms;
        var hleft=msleft/(1000*60*60); hleft=Math.floor(hleft); msleft=msleft-hleft*60*60*1000;
        var mleft=msleft/(1000*60); mleft=mleft=Math.floor(mleft); msleft=msleft-mleft*60*1000;
        var sleft=msleft/1000; sleft=Math.floor(sleft); msleft=msleft-sleft*1000;
        return hleft+"h"+mleft+"m"+sleft+"s"+msleft+"ms";
    }
    getMsLeft() {
        var nowms=Date.now();
        return this.thenms-nowms;
    }
    addTimeoutMessage(intervalno, message, msbefore, msg, deletetime) {
        var nowms=Date.now();
        var timeout=this.getMsLeft() - msbefore;
        if (timeout<0){timeout=timeout+24*60*60*1000}
        //var message = text;
        //var msgtime = new Date(nowms + timeout);
				//console.log("At " + msgtime.getHours()+"h"+msgtime.getMinutes()+"m"+msgtime.getSeconds()+"s"+msgtime.getMilliseconds()+"ms " + message);
        this.timeouts[this.timeoutno]=setTimeout(function(){
          msg.channel.send(message).then(message => message.delete(deletetime));
        },timeout);
        this.timeoutno++;

        var that=this;
        this.timeouts[this.timeoutno]=setTimeout(function(){
          that.intervals[intervalno]=setInterval(function(){
            msg.channel.send(message).then(message => message.delete(deletetime));
          },24*60*60*1000);
        },timeout);
				this.timeoutno++;
    }
    cancelCountdown(msg) {
      msg.channel.send("This is a payout, please cancel with cancelpayout instead");
    }
    cancelPayout(msg) {
        for(var i=0; i<=24; i++) {
          clearInterval(this.intervals[i]);
        }
        for(var i=0; i<=this.timeoutno; i++) {
          clearTimeout(this.timeouts[i]);
        }
				delete this.client.countdowns[this.cdid];
				msg.channel.send("Countdown cancelled");
    }
    logIntervals() {
      console.log(this.intervals);
    }
    logTimeouts() {
      console.log(this.timeouts);
    }
    getTimeoutno() {
        return this.timeoutno;
    }
    startCountdown(msg) {
        var ms = this.getMsLeft();
        var mins = ms/(60*1000);
        var hours = Math.floor(mins/60);
        var firstdel = ms%(60*60*1000);

        var deltime = 60*60*1000;
    		for(var i=1; i<=24; i++) {
            this.addTimeoutMessage(i, i + " hours left to " + this.eventname + "'s payout", i*60*60*1000-i*10, msg, deltime-i);
        }
        msg.channel.send("Countdown started. " + this.getTimeLeft() + " left to " + this.eventname + "'s payout").then(message => message.delete(firstdel));
    }
}
