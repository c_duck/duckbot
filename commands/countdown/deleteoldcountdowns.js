const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class DeleteOldCountdowns extends commando.Command {
  	constructor(client) {
    		super(client, {
    			name: 'deleteoldcountdowns',
    			aliases: ['deleteoldcds', 'deloldcds', 'doldcds'],
    			group: 'countdown',
    			memberName: 'deleteoldcountdowns',
    			description: 'Delete old finished countdowns',
    			guildOnly: true
    		});
  	}

  	async run(msg, args) {
        var serverid=msg.channel.guild.id;
        var countdowns=this.client.countdowns;
        var countdown;
        var text = "current countdowns:\n";
        for(var cdid in countdowns){
            if(typeof cdid === 'string') {
                if(countdowns[cdid].serverid==serverid) {
                    if(countdowns[cdid].getMsLeft()<0) {
                        delete this.client.countdowns[cdid];
                    }
                    else {
                        countdown=this.client.countdowns[cdid].eventname;
                        text += countdown + "\t" + this.client.countdowns[cdid].getTimeLeft() + "\n";
                    }
                }
            }
        }
        msg.channel.send("Old countdowns deleted");
        msg.channel.send(text);
  	}
}
