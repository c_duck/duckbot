var api = require('../../libraries/api');

module.exports = class CdTo {
		constructor(client, msg, eventname, totime, hoffset, countdownid, serverid) {
        this.serverid=serverid;
				this.client=client;
				this.eventname = eventname;
		    this.timeouts = [];
		    this.timeoutno = 0;
		    this.totime=totime;
		    var nowms=Date.now();
		    var now=new Date(nowms);
				var thenh=Math.floor(totime);
		    var thenm=Math.round(totime%1*100);
		    var mins=thenm-now.getMinutes();
		    var hadjust=0;
		    if(mins<=0) {
		        hadjust=-1;
		        mins=mins+60;
		    }
		    var hours=(-hoffset+hadjust+24+thenh-now.getHours())%24;
		    var offsetms=now.getSeconds()*1000+now.getMilliseconds();
		    var minutes=hours*60+mins-offsetms/(1000*60);
		    this.thenms=nowms+minutes*60*1000;
		    this.then=new Date(this.thenms);
		    this.cdid=countdownid;
		}
    getToTime() {
        return this.totime;
    }
    getTimeLeft() {
        var nowms=Date.now();
        var msleft=this.thenms-nowms;
        var hleft=msleft/(1000*60*60); hleft=Math.floor(hleft); msleft=msleft-hleft*60*60*1000;
        var mleft=msleft/(1000*60); mleft=mleft=Math.floor(mleft); msleft=msleft-mleft*60*1000;
        var sleft=msleft/1000; sleft=Math.floor(sleft); msleft=msleft-sleft*1000;
        return hleft+"h"+mleft+"m"+sleft+"s"+msleft+"ms";
    }
    getMsLeft() {
        var nowms=Date.now();
        return this.thenms-nowms;
    }
    addTimeoutMessage(message, msbefore, msg, deletetime) {
        var nowms=Date.now();
        var timeout=this.getMsLeft() - msbefore;
        //var message = text;
        //var msgtime = new Date(nowms + timeout);
				//console.log("At " + msgtime.getHours()+"h"+msgtime.getMinutes()+"m"+msgtime.getSeconds()+"s"+msgtime.getMilliseconds()+"ms " + message);
				if(deletetime==undefined){
					this.timeouts[this.timeoutno]=setTimeout(function(){msg.channel.send(message);},timeout);
				}
				else {
					this.timeouts[this.timeoutno]=setTimeout(function(){msg.channel.send(message).then(message => message.delete(deletetime));},timeout);
				}
				this.timeoutno++;
    }
    cancelCountdown(msg) {
        for(var i=0; i<=this.timeoutno; i++) {
            clearTimeout(this.timeouts[i]);
        }
				delete this.client.countdowns[this.cdid];
				msg.channel.send("Countdown cancelled");
    }
    getTimeoutno() {
        return this.timeoutno;
    }
    startCountdown(mention, msg) {
        var ms = this.getMsLeft();
        var mins = ms/(60*1000);
        var hours = Math.floor(mins/60);

				var mentionstring = "";
        if(mention.toLowerCase()=="everyone" || mention.toLowerCase()=="yes"){
					mentionstring="@everyone ";
				}
				else {
					if(mention.toLowerCase() !== "no") {
	          var users = api.GetUsersFromParms(msg, mention);
							if(users.length == 1){
								mentionstring = users[0].user+ " ";
							} else if(users.length > 1){
								var response = "multiple users found, please choose one:";
								for(var i=0;i<users.length;i++){
									var user = users[i];
									response += "\n" + user.username;
								}
								msg.channel.send(response);
							} else {
								msg.channel.send("No user " + mention + " found!");
							}
					}
				}

        var deltime = 60*60*1000;
    		for(var i=1; i<=hours; i++) {
            if(i==hours){deltime=30*60*1000}
            this.addTimeoutMessage(i + " hours left to " + this.eventname, i*60*60*1000, msg, deltime);
        }
        if(mins>30) {this.addTimeoutMessage("30 minutes left to " + this.eventname, 30*60*1000, msg, 20*60*1000);}
        if(mins>10) {this.addTimeoutMessage(mentionstring + "10 minutes left to " + this.eventname, 10*60*1000, msg, 10*60*1000);}
        this.addTimeoutMessage(mentionstring + this.eventname + " starts now", 0, msg);
        msg.channel.send("Countdown started. " + this.getTimeLeft() + " left to " + this.eventname).then(message => message.delete(ms));
    }
}
