const commando = require('discord.js-commando');
const Tout = require('./tout.js');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Timeoutmessage extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'timeoutmessage',
			aliases: ['tom'],
			group: 'countdown',
			memberName: 'timeoutmessage',
			description: 'Adds a reocurring message with a specific frequency',
			details: 'Adds a reocurring message. Use 6 arguments: \n1. message - (string) \n2. message time - UTC time \n3. Delay in days before starting timeout message \n4. duration for message \n5. Timeout time for the message \n6. ms lag. To get messages in order use the lowest value for the one you want first',
            args: [
                {
                    key: 'message',
                    label: 'message (string)',
                    prompt: 'Please input a message (string)',
                    type: 'string'
                },
                {
                    key: 'messageTime',
                    label: 'messageTime (number)',
                    prompt: 'Please input a messageTime',
                    type: 'float',
                    min: 0,
                    max: 24
                },
                {
                    key: 'dayDelay',
                    label: 'dayDelay (number)',
                    prompt: 'Please input a dayDelay',
                    type: 'float',
                    min: 0,
                    max: 10
                },
                {
                    key: 'duration',
                    label: 'duration (number)',
                    prompt: 'Please input a message duration time in hours',
                    type: 'float',
                    min: 0,
                    max: 24
                },
                {
                    key: 'timeoutTime',
                    label: 'timoutTime (number)',
                    prompt: 'Please input a timout time in hours',
                    type: 'float',
                    min: 0,
                    max: 240
                },
                {
                    key: 'mslag',
                    label: 'mslag (number)',
                    prompt: 'Please input a mslag time in ms',
                    type: 'float',
                    min: 0,
                    max: 60000
                }
            ],
            argsSingleQuotes: true,
            argsCount: 6,
            guildOnly: true
		});
	}

	async run(msg, args) {
		var message = args.message;
		var messagetime = args.messageTime;
    var daydelay = args.dayDelay;
    var timeouttime = args.timeoutTime;
    var duration = args.duration;
    var mslag = args.mslag;
    var cdid = msg.guild.id + "_" + message;
		var serverid=msg.guild.id;
		console.log('message ' + message);
		console.log('messagetime ' + messagetime);
		console.log('daydelay ' + daydelay);
		console.log('timeouttime ' + timeouttime);
		console.log('duration ' + duration);
		console.log('mslag ' + mslag);
    //															 constructor(client, msg, name, messagetime, daydelay, mslag, duration, timeouttime, timeoutid, serverid)
		this.client.countdowns[cdid] = new Tout(this.client, msg, message, messagetime, daydelay, timeouttime, duration, mslag, cdid, serverid);
		this.client.countdowns[cdid].startTimeout(msg);
		this.client.countdowns[cdid].guildId=serverid;
		this.client.countdowns[cdid].guildName=msg.guild.name;
	}

}
