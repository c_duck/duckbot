const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Version extends commando.Command {
		constructor(client) {
				super(client, {
					name: 'version',
					group: 'admin',
					memberName: 'version',
					description: 'get the git commit the bot is running on (requires permission)',
					details: 'get the git commit the bot is running on (requires permission)',
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {

			var spawn = require('child_process').spawn;
			var log = function(err,stdout,stderr){
					if(stdout){console.log(stdout);}
					if(stderr){console.log(stderr);}
			};
			var commit = spawn('git', ['log','-n','1']);
      commit.stdout.on('data', function(data) {
          msg.channel.send(data.toString());
					console.log(data.toString());
      });
		}
}
