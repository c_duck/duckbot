const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class GuildMembers extends commando.Command {
		constructor(client) {
				super(client, {
						name: 'guildmembers',
						aliases: ['gm'],
						group: 'admin',
            description: '',
						memberName: 'guildmembers',
						guildOnly: true
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {
          //console.log(msg.guild.members);
          //console.log(members);
          var collection = msg.guild.members;
          //var memberids = collection.keyArray();
          var users = collection.array();
          var text="Guild members:"
          for(var i=0; i<users.length; i++) {
            text+="\n" + users[i].user.username;
          }
          msg.author.send(text);
    }
}
