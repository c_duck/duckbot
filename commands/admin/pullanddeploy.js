const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class PullAndDeploy extends commando.Command {
		constructor(client) {
				super(client, {
					name: 'pullanddeploy',
					aliases: ['pad'],
					group: 'admin',
					memberName: 'pullanddeploy',
					description: 'git pull master from (requires permission)',
					details: 'git pull master (requires permission)',
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {
				msg.channel.send("fetching updates...").then(function(sentMsg){
	      console.log("updating...");
	      var spawn = require('child_process').spawn;
		        var log = function(err,stdout,stderr){
		            if(stdout){console.log(stdout);}
		            if(stderr){console.log(stderr);}
		        };
		        var fetch = spawn('git', ['fetch']);
		        fetch.stdout.on('data',function(data){
		            console.log(data.toString());
		        });
		        fetch.on("close",function(code){
		            var reset = spawn('git', ['reset','--hard','origin/master']);
		            reset.stdout.on('data',function(data){
		                console.log(data.toString());
		            });
		            reset.on("close",function(code){
		                var npm = spawn('npm', ['install']);
		                npm.stdout.on('data',function(data){
		                    console.log(data.toString());
		                });
		                npm.on("close",function(code){
		                    console.log("goodbye");
		                    sentMsg.edit("brb!").then(function(){
		                        client.destroy().then(function(){
		                            process.exit();
		                        });
		                    });
		                });
		            });
		        });
	    	});
		}
}
