const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class AdminDeleteOldCountdowns extends commando.Command {
		constructor(client) {
				super(client, {
					name: 'admindeleteoldcountdowns',
					aliases: ['admindeleteoldcds', 'admindeloldcds', 'admindoldcds', 'adeleteoldcds', 'adeloldcds', 'adoldcds'],
					group: 'admin',
					memberName: 'admindeleteoldcountdowns',
					description: 'Delete old finished countdowns in all guilds (requires permission)',
					details: 'Delete old finished countdowns in all guilds (requires permission)',
					guildOnly: true
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {
		    var countdowns=this.client.countdowns;
				var text = "current countdowns:\n";
		    for(var cdid in countdowns){
			        if(typeof cdid === 'string') {
									if(this.client.countdowns[cdid].cdid !== undefined) {
			                if(countdowns[cdid].getMsLeft()<0) {
			                    delete this.client.countdowns[cdid];
			                }
											else {
													text += cdid + "\t" + this.client.countdowns[cdid].getTimeLeft() + "\n";
											}
									}
			        }
		    }
		    msg.channel.send("Old countdowns deleted");
				msg.channel.send(text);
		}
}
