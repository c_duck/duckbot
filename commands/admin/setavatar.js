const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class SetAvatar extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'setavatar',
			group: 'admin',
			memberName: 'setavatar',
			description: 'Change the avatar of the bot'
		});
	}

	async run(msg, args) {
		if (args=='1') {
			msg.channel.send('avatar1 set');
			msg.client.user.setAvatar('./avatar1.jpg');
		}
		else if (args=='2'){
			msg.channel.send('avatar2 set');
			msg.client.user.setAvatar('./avatar2.jpg');
		}
	}
}
