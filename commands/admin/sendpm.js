const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class SendPMtoChannel extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'sendpmtochannel',
			aliases: ['pmchannel'],
			group: 'utility',
			memberName: 'sendpmtochannel',
			description: 'Bot sends pm to all non-bot users in the channel',
			details: '!pmchannel message'
		});
	}

	async run(msg, args) {
		console.log(args);
		if (this.client.options.admins.indexOf(msg.author.id) > -1){
			var users = msg.channel.members.array();
			for(var x in users){
				var member = users[x];
				if  (!member.user.bot) {
					if (args.length>0) {
						member.user.send(args);
					}
					else {msg.channel.send('please add message to send')}
					console.log(member.user.username);
				}
			}
		}
		else {
			msg.channel.sendMessage('you do not have permission to use this command');
		}
	}
}
