const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class AdminCancelCountdown extends commando.Command {
		constructor(client) {
				super(client, {
						name: 'admincancelcountdown',
						aliases: ['admincancelcd', 'admincancel', 'acancelcd', 'acancel'],
						group: 'admin',
						memberName: 'admincancelcountdown',
						description: 'Cancel a specific countdown in any guild (requires permission)',
						details: 'Cancel a specific countdown in any guild (requires permission)',
						args: [
								{
									key: 'id',
									label: 'countdown id',
									prompt: 'Please input a countdown id that you want to cancel',
									type: 'string'
								}
						],
						guildOnly: true
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {
				msg.channel.send("Args " + args.event);
        var cdid=args.id;
        if(this.client.countdowns[cdid]===undefined) {
           msg.channel.send("No such countdown exist");
        }
        else {
            this.client.countdowns[cdid].cancelCountdown();
            msg.channel.send(args.id + " countdown cancelled");
        }
		}
}
