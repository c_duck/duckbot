const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class AdminCurrentCountdowns extends commando.Command {
		constructor(client) {
				super(client, {
						name: 'admincurrentcountdowns',
						aliases: ['admincurrentcds', 'admincurrcds', 'acurrentcds', 'acurrcds'],
						group: 'admin',
						memberName: 'admincurrentcountdowns',
						description: 'Get current countowns in all guilds (requires permission)',
						details: 'Get current countowns in all guilds (requires permission)',
						guildOnly: true
				});
		}

		hasPermission(msg) {
			return msg.author.id === this.client.options.owner;
		}

		async run(msg, args) {
				var text = "current countdowns:\n";
		    for(var cdid in this.client.countdowns){
		        if(typeof cdid === 'string') {
		            if(this.client.countdowns[cdid].cdid !== undefined) {
		                text += cdid + "\t" + this.client.countdowns[cdid].guildName + "\t" + this.client.countdowns[cdid].getTimeLeft() + "\n";
								}
		        }
		    }
		    msg.channel.send(text);
		}
}
