const commando = require('discord.js-commando');


//https://api.imgflip.com/popular_meme_ids
var meme = {
    "brace": 61546,
    "mostinteresting": 61532,
    "fry": 61520,
    "onedoesnot": 61579,
    "yuno": 61527,
    "success": 61544,
    "allthethings": 61533,
    "doge": 8072285,
    "drevil": 40945639,
    "skeptical": 101711,
    "notime": 442575,
    "yodawg": 101716,
    "awkwardpenguin": 61584
};


module.exports = class Meme extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'meme',
			group: 'fun',
			memberName: 'meme',
			description: `returns a meme: meme/memeid 'top text' 'bottom text'`,
            details: `returns a meme: meme/memeid 'top text' 'bottom text'`,
            examples: [`!meme fry 'top text' 'bottom text'`, `!meme 61520 'top text' 'bottom text'`],
			args: [
				{
					key: 'meme',
					label: 'meme/memeid',
					prompt: 'Which meme do you want?',
					type: 'string'
				},
                {
                    key: 'top',
                    label: `top text ('within single quotes if more than one word')`,
                    prompt: 'What text do you want at the top of the meme?',
                    type: 'string'
                },
                {
                    key: 'bottom',
                    label: `bottom text ('within single quotes if more than one word')` ,
                    prompt: 'What text do you want at the bottom of the meme?',
                    type: 'string'
                }
			],
            argsSingleQuotes: true,
            argsCount: 3
		});
	}

	async run(msg, args) {
        var memetype = args.meme;
        var Imgflipper = require('imgflipper');
        var imgflipper = new Imgflipper(this.client.AuthDetails.imgflip_username, this.client.AuthDetails.imgflip_password);
        if(isNaN(memetype)) {
          if(memetype in meme) {
            imgflipper.generateMeme(meme[memetype], args.top, args.bottom, function(err, image){
                //console.log(arguments);
                msg.channel.send(image);
            });
          }
          else {
            msg.channel.send("cannot find meme");
          }
        } else {
            imgflipper.generateMeme(memetype, args.top, args.bottom, function(err, image){
                //console.log(arguments);
                msg.channel.send(image);
            });
        }
	}
}
