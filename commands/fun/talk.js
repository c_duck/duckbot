const commando = require('discord.js-commando');
var cleverbot = require("cleverbot-node");
talkbot = new cleverbot;
cleverbot.prepare(function(){});

module.exports = class Talk extends commando.Command {
		constructor(client) {
				super(client, {
					name: 'talk',
					group: 'fun',
					memberName: 'talk',
					description: 'cleverbot respond to your message',
		      		details: 'cleverbot responds to any string'
				});
		}

		async run(msg, args) {
			talkbot.write(args, function (response) {
			msg.channel.send(response.message)
			})
		}
}
