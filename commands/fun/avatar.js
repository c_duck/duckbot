const commando = require('discord.js-commando');
var api = require('../../libraries/api');

module.exports = class Avatar extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'avatar',
			aliases: ['av'],
			group: 'fun',
			memberName: 'avatar',
			description: 'post the avatar pic of a user',
      details: '!avatar username',
			argsType: 'single'
		});
	}

	async run(msg, args) {
    var response;
    var users = api.GetUsersFromParms(msg, args);
      if(users.length == 1){
        console.log(users[0].user);
        response = users[0].user.avatarURL;
        msg.channel.send(response);
      } else if(users.length > 1){
        //msg.channel.send('multiple users found');
        response = "multiple users found, please choose one:";
        console.log(users);
        for(var i=0;i<users.length;i++){
          var uservar = users[i];
          response += "\n" + uservar.user.username;
        }
        msg.channel.send(response);
      } else {
        msg.channel.send("No user " + args + " found!");
      }
	}
}
