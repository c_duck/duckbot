const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Recruit extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'recruit',
			group: 'utility',
			memberName: 'recruit',
			description: 'Perform commands on recruits',
			details: 'Enter new recruits swgoh.gg profile name',
			args: [
					{
							key: 'recruit',
							label: 'command to execute (fresh guild. update guild info)',
							prompt: 'Please input command',
							type: 'string'
					}
			]
		});
	}

	async run(msg, args) {

		if(args) {
            var recruit = args.recruit;

			console.log(recruit);

			if(recruit && recruit.length > 0) {
                api.GetRecruitCharacters(msg, recruit);
                api.GetRecruitShips(msg, recruit);
			} else {
				msg.channel.send("please enter a swgoh profile to look up");
			}		
            
		} else {
			msg.channel.send("no input parameters defined");
		}
		
	}

}


