const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Toon extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'guild',
			group: 'utility',
			memberName: 'guild',
			description: 'Perform commands on guilds',
			details: 'For a list of shortnames use !toons',
			args: [
					{
							key: 'command',
							label: 'command to execute (fresh guild. update guild info)',
							prompt: 'Please input command',
							type: 'string'
					},
					{
							key: 'guild',
							label: 'guild to execute commands against',
							prompt: 'please input guild (vm, vs, vc, vws, vq, opp)',
							type: 'string',
							default: 'none'
					},
					{
							key: 'link',
							label: 'link to update',
							prompt: 'please input guild (vm, vs, vc, vws, vq, opp)',
							type: 'string',
							default: 'none'
					}
			]
		});
	}

	async run(msg, args) {

		if(args) {
            var guild = args.guild;
		    var cmd = args.command;
			var link = args.link == 'none' ? null: args.link;

			console.log(guild);
			console.log(cmd);
			console.log(link);

			if(cmd === "refresh") {
				switch(guild){
					case "vm":
					case "vs":
					case "vq":
					case "vc":
					case "vws":
					case "opp":
						//valid guilds. retrieve from firebase
						api.RefreshGuildSync(msg, guild);
						break;
					default:
						//not a valid guild. return message to user
						msg.channel.send("invalid guild. please enter a valid guild abbreviation");
						break;
				}
			} else if(cmd === "update") {
				if(link) {
					api.UpdateGuildLink(msg, guild, link)
				} else {
					msg.channel.send("please provide the updated link for " + guild);
				}
			} else {
				msg.channel.send("please use a valid command (update, refresh)");
			}		
            
		} else {
			msg.channel.send("no input parameters defined");
		}
		
	}

}


