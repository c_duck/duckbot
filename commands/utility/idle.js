const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Idle extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'idle',
			group: 'utility',
			memberName: 'idle',
			description: 'sets bot status to idle',
			details: 'sets bot status to idle'
		});
	}

	async run(msg, args) {
		this.client.user.setStatus('idle');
	}
}
