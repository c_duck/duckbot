const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class MyId extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'myid',
			group: 'utility',
			memberName: 'myid',
			description: 'returns the user id of the sender',
			details: 'returns the user id of the sender'
		});
	}

	async run(msg, args) {
		msg.channel.send(msg.author.id);
	}
}
