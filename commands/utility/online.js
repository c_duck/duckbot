const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Online extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'online',
			group: 'utility',
			memberName: 'online',
			description: 'sets bot status to online',
			details: 'sets bot status to online'
		});
	}

	async run(msg, args) {
		this.client.user.setStatus('online');
	}
}
