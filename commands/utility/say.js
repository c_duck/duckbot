const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Say extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'say',
			group: 'utility',
			memberName: 'say',
			description: 'Bot says message',
			details: 'Bot says message'
		});
	}

	async run(msg, args) {
		msg.channel.send(args);
	}
}
