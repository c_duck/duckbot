const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Toons extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'toons',
			group: 'utility',
			memberName: 'toons',
			description: 'Get a pm with all the toon shortnames'
		});
	}

	async run(msg) {
		api.GetToonNames(msg);
	}
}
