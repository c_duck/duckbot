const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Toon extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'opponent',
			group: 'utility',
			memberName: 'opponent',
			description: 'Update the opponent information for TW opponent with !opponent [url]',
			details: 'Update the opponent information for TW opponent',
			args: [
					{
							key: 'link',
							label: 'link to update opponent',
							prompt: 'Please input link',
                            type: 'string',
                            default: undefined
					}
			]
		});
	}

	async run(msg, args) {

		if(args) {
            //guild from command line
            var link = args.link;
            
            if(link) {
                //https://swgoh.gg/g/[id]/[guild]/
                //https://swgoh.gg/g/304/ventress-moon/zetas/
                //https://swgoh.gg/api/guilds/304/units/
            } else {
                msg.channel.send("opponent link was not supplied please use !opponent https://url.gg/to/opponent/guild")
            }
		} else {
			msg.channel.send("no input parameters defined");
		}
		
	}

}


