const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Zetas extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'zetas',
			group: 'utility',
			memberName: 'zetas',
			description: 'Get information on toons that have zetas in the guild where the command is executed',
			details: 'For a list of shortnames use !toons',
			args: [
					{
							key: 'toon',
							label: 'toon (string)',
							prompt: 'Please input toon',
							type: 'string'
					},
					{
							key: 'arg1',
							label: 'toon level or guild name',
							prompt: 'this should never happen but needed for some reason',
							type: 'string',
							default: '1'
					},
					{
							key: 'arg2',
							label: 'toon level or guild name',
							prompt: 'this should never happen but needed for some reason',
							type: 'string',
							default: 'detect'
					}
			],
			guildOnly: true
		});
	}

	async run(msg, args) {
		
		var toon = util.StripSpecialCharacters(args.toon);
		var arg1 = util.StripSpecialCharacters(args.arg1);
		var arg2 = util.StripSpecialCharacters(args.arg2);
		var stars = 1;
		var guild = '';
		
		if(args) {
			stars = api.GetStarsFromParms(arg1, arg2);
			guild = api.GetGuildFromParms(msg.guild.name, arg1, arg2);
			guild = util.GetGuildBucketName(guild);
			
			if(stars > 0) {			
				api.GetToons(msg, toon, guild, stars, true);
			}
			else {
				msg.channel.send('wrong input arguments');
			}
			
		} else {
			msg.channel.send("no input parameters defined");
		}
	}

}


