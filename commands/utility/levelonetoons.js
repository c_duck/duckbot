const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class LevelOneToons extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'levelonetoons',
			aliases: ['l1', 'l1toons', 'lonetoons', 'lone', 'levelone'],
			group: 'utility',
			memberName: 'levelonetoons',
			description: 'Get level one toons'
		});
	}

	async run(msg, args) {
		var request = require('request');
		if (args==''){
			var guildName = msg.guild.name;
			var guild = guildName.replace("Ventress ","").toLowerCase();
		}
		else {
			guild=args;
		}

		if (guild=='moon' || guild=='sun' || guild=='qui') {

			var url = "https://ventress-19de0.firebaseio.com/" + guild + ".json?orderBy=%22ID%22&auth=ywie1Mgo3dBrqOOxi56HC8AHFe5pkIE9NbRt9JNn";

			request(url, function (error, response, body) {
			  var Toons = JSON.parse(body);
				var dict={}
				var i=0
				for (var i=0; i<Toons.length;i++){
					var x = Toons[i].Players
					if(x!=undefined) {
						for (var player=0; player<x.length; player++){
							if(x[player].ToonLevel==1){
								if (dict[x[player].PlayerName] >0){
									dict[x[player].PlayerName]+=1
								}
								else {
									dict[x[player].PlayerName]=1
								}
							}
						}
					}
				}
				var sortable = [];
				for (var player in dict) {
				    sortable.push([player, dict[player]]);
				}

				sortable.sort(function(a, b) {
				    return b[1] - a[1];
				});
				//console.log(sortable)
				msg.channel.send(sortable)
			});
		}
	}
}
