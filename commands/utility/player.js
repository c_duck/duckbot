const commando = require('discord.js-commando');
var api = require('../../libraries/api');
var util = require('../../libraries/util');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Player extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'player',
			group: 'utility',
			memberName: 'player',
			description: 'Get information on players zetas',
			details: 'For a list of zetas a guild mate has use !player zetas guild playername',
			args: [
                {
                    key: 'command',
                    label: 'command to execute on a player(zetas, more coming...)',
                    prompt: 'Please input command',
                    type: 'string'
            },
            {
                    key: 'guild',
                    label: 'guild to execute commands against',
                    prompt: 'please input guild (vm, vs, vc, vws, vq, opp)',
                    type: 'string',
                    default: 'none'
            },
            {
                    key: 'player',
                    label: 'player to look up',
                    prompt: 'please input player name as you see them in the results of toon searching and others',
                    type: 'string',
                    default: 'none'
            }
			],
			guildOnly: true
		});
	}

	async run(msg, args) {

		if(args) {
            var guild = args.guild;
		    var cmd = args.command;
			var player = args.player;

			console.log(guild);
			console.log(cmd);
			console.log(player);

			if(cmd === "zetas") {
				switch(guild){
					case "vm":
					case "vs":
					case "vq":
					case "vc":
					case "vws":
					case "opp":
						//valid guilds. retrieve from firebase
						api.GetPlayerZetas(msg, guild, player);
						break;
					default:
						//not a valid guild. return message to user
						msg.channel.send("invalid guild. please enter a valid guild abbreviation");
						break;
				}
			} else {
				msg.channel.send("please use a valid command (zetas)");
			}		
            
		} else {
			msg.channel.send("no input parameters defined");
		}
		
	}

}


