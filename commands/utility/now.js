const commando = require('discord.js-commando');
var moment = require('moment-timezone');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Now extends commando.Command {
    constructor(client) {
    	super(client, {
    		name: 'now',
    		group: 'utility',
    		memberName: 'now',
    		description: 'Posts current time for CET and MST',
            details: 'Posts current time for CET and MST'
    	});
    }

    async run(msg, args) {

        msg.channel.send(moment.tz().format('HH:mm:ss z'));
        var cet = moment.tz('Europe/Stockholm');
        msg.channel.send(cet.format('HH:mm:ss z'));
        var mst = cet.tz('America/Edmonton');
        msg.channel.send(mst.format('LTS z'));

        var now = moment.utc();
        // get the zone offsets for this time, in minutes
        var offset1 = moment.tz.zone("America/Edmonton").offset(now);
        var offset2 = moment.tz.zone("Europe/Stockholm").offset(now);

    }
}
