const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class Hello extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'hello',
			group: 'utility',
			memberName: 'hello',
			description: 'Duckbot says hello',
			details: 'Duckbot says: "Hello world! **Join the duck side!**"'
		});
	}

	async run(msg, args) {
		msg.channel.send("Hello world! **Join the duck side!**");
	}
}
