const commando = require('discord.js-commando');

// https://discord.js.org/#/docs/commando/master/class/Command

module.exports = class DeleteMessages extends commando.Command {
	constructor(client) {
		super(client, {
			name: 'deletemessages',
			aliases: ['dm'],
			group: 'utility',
			memberName: 'deletemessages',
			description: 'Bot deletes messages',
			details: 'Bot deletes last x messages',
			args: [
					{
							key: 'number',
							label: 'number of messages to delete',
							prompt: 'Please input number of messages to delete',
							type: 'integer',
							min: 1,
							max: 100
					},
			],
			argsCount: 1,
			guildOnly: true
		});
	}

	async run(msg, args) {
		var numtodelete = Number(args.number) + 1;
		msg.channel.bulkDelete(numtodelete);
	}
}
